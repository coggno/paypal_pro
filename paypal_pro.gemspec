$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "paypal_pro/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "paypal_pro"
  spec.version     = PaypalPro::VERSION
  spec.authors     = ["Sergei Rogovskiy", "Alexey Bobr"]
  spec.email       = ["Alexi@coggno.com"]
  spec.homepage    = ""
  spec.summary     = "paypal pro ruby wrapper for active merchant"
  spec.description = "paypal pro ruby wrapper for active merchant"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]


end
