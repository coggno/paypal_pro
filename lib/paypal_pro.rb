require "paypal_pro/railtie"
require "paypal_pro/config"
require "paypal_pro/model"
require "paypal_pro/controller"

module PaypalPro
  ActiveRecord::Base.class_eval do
    include PaypalPro::Model
  end

  ActionController::Base.class_eval do
    include PaypalPro::Controller
  end
end
