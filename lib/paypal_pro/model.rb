# TODO refactor *100 - it is odd
module PaypalPro
  
  # The module adds paypal processing methods to models which represent 
  # some sort of e-commerce orders.
  # 
  # Three new methods are:
  # * +process_payment+ - processes payment with credit card
  # * +process_express_payment+ - initiates express checkout 
  # * +complete_express_payment+ - completes express checkout
  module Model 
    
    def self.included(base)
      base.extend(ActsMod)
    end
    
    module ActsMod
      # Adds paypal order behavior to a model class.
      # 
      # * +total_method+ - total cost method name
      # * +description_method+ - order description name
      def acts_as_paypal_order(total_method, description_method, currency_method = nil)
        class_eval do

          class_attribute(:total_method,        default: total_method)
          class_attribute(:description_method,  default: description_method)
          class_attribute(:currency_method,     default: currency_method) if currency_method

          include PaypalPro::Model::InstanceMethods
        end  
      end
    end 
    
    module InstanceMethods #:nodoc:
      
      include ActiveMerchant::Billing
      
      # Processes payment with provided credit card information
      # 
      # * +card+ - credit card information
      # * +address+ - billing and shipping address information
      def process_payment(card, address)
        gateway = create_gateway
        #    gateway.connection.wiredump_dev = STDERR
        desc = self.send(self.class.description_method)
        desc = desc[0..126] if desc.length > 127 
        params = {
          :description => desc ,
          :ip => ip,
          :address => {
            :name => "#{card.first_name} #{card.last_name}",
            :address1 => address[:address],
            :city => address[:city],
            :state => address[:state],
            :country => address[:country],
            :zip => address[:zip]
          }
        }
        if self.class.respond_to?(:currency_method)
           params[:currency] = self.send(self.class.currency_method)
        end
        logger.warn("debug process_payment")
        logger.warn(self.send(self.class.total_method) * 100)
        logger.warn(params.inspect)
        res = gateway.purchase(self.send(self.class.total_method) * 100, card, params) # total in cents
        if res.success?
	  save
	else
	  logger.warn("Paypal failed: #{res.inspect}")
        end
        res
      end
      
      # Sends initial payment setup request to paypal express checkout system
      # 
      # * +success_url+ - callback redirect in case of successful payment approval
      # * +falure_url+ - callback redirect in case of payment cancelation or falure
      def process_express_payment(success_url, falure_url)
        gateway = create_express_gateway
        desc = self.send(self.class.description_method)
        desc = desc[0..126] if desc.length > 127
        if self.class.respond_to?(:currency_method)
           currency = self.send(self.class.currency_method)
        end

        response = gateway.setup_purchase(
                                          self.send(self.class.total_method) * 100,
        :order_id => id,
        :return_url => success_url,
        :cancel_return_url => falure_url,
        :description => desc,
        :currency => currency
        )
        if response.success?
          gateway.redirect_url_for(response.params['token'])
        end
      end
      
      # Completes express checkout after paypal said ok 
      # 
      # * +token+ - Paypal token
      # * +payer_id+ - Paypal payer ID
      def complete_express_payment(token, payer_id)
        gateway = create_express_gateway
        response = gateway.details_for(token)
        success = false
        if response.success?
          if self.class.respond_to?(:currency_method)
            currency = self.send(self.class.currency_method)
          end
          response = gateway.purchase(self.send(self.class.total_method) * 100, :express => true,
          :token => token, :payer_id => payer_id, :currency => currency)
          #      puts response.message
          #      puts response.success?
          success = response.success?
        end
        success
      end
      
      protected
      
      def create_gateway
        Base.gateway_mode = PaypalPro::Config::mode
        PaypalGateway.pem_file = PaypalPro::Config::pem_file 
        PaypalGateway.new(:login => PaypalPro::Config::login, :password => PaypalPro::Config::password)
      end
      
      def create_express_gateway
        Base.gateway_mode = PaypalPro::Config::mode
        PaypalExpressGateway.pem_file = PaypalPro::Config::pem_file 
        PaypalExpressGateway.new(:login => PaypalPro::Config::login, :password => PaypalPro::Config::password)
      end
    end
  end
end
