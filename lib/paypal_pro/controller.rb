require "active_merchant/billing/rails"

module PaypalPro  #:nodoc:
  # The module introduces 3 actions to perform paypal checkout process.
  # * +checkout+
  # * +paypal_cancel+
  # * +paypal_success+
  module Controller

    def self.included(base)
      base.extend(ActsMod)
    end

    module ActsMod #:nodoc:
      def acts_as_paypal_checkout_controller
        class_eval do
          include PaypalPro::Controller::InstanceMethods
        end
      end
    end

    module InstanceMethods #:nodoc:

      include ActiveMerchant::Billing

      def checkout      
        resp = {}
                
        unless params[:payment]
          render json: {success: false, message: 'Please select payment type'}
          return 
        end

        card = CreditCard.new({:first_name => current_login.first_name, :last_name => current_login.last_name}.merge(card_params || {}))
        unless card.valid?
          render json: {success: false, message: card.errors.full_messages.join(', ')}, status: 422
          return
        end
        @order = build_order
        @order.order_type = 'card'
                
        if @order.respond_to?('new_record?') && !@order.new_record?
          @order.logger.warn("Order is saved in the database prior to Paypal processing. It may allow incomplete transactions to be committed")
        end

        res = @order.process_payment(card, current_login.card_billing_address)
        #res =  ActiveMerchant::Billing::Response.new(false, "This transaction cannot be processed.", {:correlation_id=>"6e154e97a2c02", :version=>"59.0", :amount_currency_id=>"USD", :cvv2_code=>"N", :ack=>"Failure", :avs_code=>"N", :timestamp=>"2014-01-30T21:10:44Z", :error_codes=>"15005", :amount=>"12.95", :build=>"8943548", :message=>"This transaction cannot be processed."}, { :avs_result => {:postal_match=>"N", :street_match=>"N", :code=>"N", :message=>"Street address and postal code do not match."},  :cvv_result => "N" })
        unless res.success?
          pf = {
            :login_id => @order.login_id, 
            :error_code => res.to_json,
            :total => @order.total,
            :payment_type => @order.class.to_s
          }
          if @order.respond_to? :order_items
            pf[:cart_items] = @order.order_items.collect{|i| {:doc => i.document_id, :amount => i.amount}}.to_json 
          end
          PaypalFail.create(pf)
          @order.logger.warn("PAYPAL: #{res.message}") if @order.logger
        end
        if res.success?
          do_complete_order(@order, res)          
        else
          do_cancel_order(@order, res)
        end
      end

      protected

      def card_params
        ps = params.fetch(:card).permit(:verification_value, :month, :year, :expiry, :first_name, :last_name, :name, :number)
        if ps[:expiry].present?
          month, year = ps[:expiry].split('/')
          year = ("20" + year) if year.size == 2
          ps.except(:expiry).merge({month: month, year: year})
        else
          ps.except(:expiry)
        end
      end
            
      def build_order
        'Please overwrite build_order method in your controller'
      end

      def cancel_order(order, res = {})
        flash[:notice] = 'Please overwrite cancel_order method in your controller'
      end

      def complete_order(order, res = {})
        flash[:notice] = 'Please overwrite complete_order method in your controller'
      end

      private

      def do_cancel_order(order, res)
        cancel_order(order, res)
      end

      def do_complete_order(order, res)
        order.save!
        # TemporaryOrder.remove(session[:current_order])
        # session[:current_order] = nil
        # session[:current_order_time] = nil
        complete_order(order, res)
      end

    end
  end

end
