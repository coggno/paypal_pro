require 'active_support'

module PaypalPro

  module Config
    @@pem_file = nil
    @@login = nil
    @@password = nil
    @@mode = nil
    @@default_billing_address = nil

    mattr_accessor :default_billing_address
    mattr_accessor :pem_file
    mattr_accessor :login
    mattr_accessor :password
    mattr_accessor :mode
     
  end
  
end